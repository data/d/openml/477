# OpenML dataset: fl2000

https://www.openml.org/d/477

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

County data from the 2000 Presidential Election in Florida.

Compiled by Brett Presnell
Department of Statistics, University of Florida

These data are derived from three sources, described below.  As far
as I am aware, you are free to use these data in any way that you
see fit, though some acknowledgement is always nice.

The candidate vote counts are the final certified counts reported
by the Florida Division of Elections.  These were obtained from
the NORC web site in the file Cert_results.csv.  Note that these
do NOT inculde the federal absentee votes (so that Gore's total
vote is actually higher here than Bush's).

The undervote and overvote counts were extracted from the NORC
ballot level data in the file aligned.txt.  Since aligned.txt is
too large to work with in R (or almost any other program) I used
cut (a standard UNIX program) to extract just the columns I needed:

cut -f 2,9,10 -d"|" aligned.txt  > tmp

Then I read the results into R and processed them there.

The technology and columns data were extracted from the Media
Group data from the NORC web site.  "Technology" is simply the
type of voting machine used, and "columns" is 1 if the ballot
listed the presidential candidates in a single column on a single
page, and 2 if the presidential candidates were spread over two
columns or two pages of the ballot.

These agree with some earlier data that I had obtained from the NY
Times web site, except that in the media group data the PalmBeach
county ballot (the famous butterfly ballot) was listed as having
one column.  I would definitely call this a two-column ballot, so
that is the designation recorded here.  At one time I thought that
MiamiDade County also used a two-column ballot, but I was wrong
(the ballot listed the candidates and parties in English and
Spanish in opposing columns).  Images of most of the ballots can
be found on the New York Times web site:
www.nytimes.com/images/2001/11/12/politics/recount/index_BALLOT.html



Information about the dataset
CLASSTYPE: nominal
CLASSINDEX: 2

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/477) of an [OpenML dataset](https://www.openml.org/d/477). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/477/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/477/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/477/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

